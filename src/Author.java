public class Author {

        private final FcsAuthor fcs;
        private String creativePseudonym;

        public FcsAuthor getFcs() {
            return new FcsAuthor(fcs.firstname, fcs.secondname, fcs.surname);
        }

        public String getCreativePseudonym() {
            return creativePseudonym;
        }

        public Author(FcsAuthor fcs, String creativePseudonym) {
            this.fcs = fcs;
            this.creativePseudonym = creativePseudonym;
        }

        public static class FcsAuthor {
            private final String firstname;
            private final String secondname;
            private final String surname;

            public FcsAuthor(String firstname, String secondname, String surname) {
                this.firstname = firstname;
                this.secondname = secondname;
                this.surname = surname;
            }
        }
}
