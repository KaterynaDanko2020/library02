import java.util.Date;

public class User {
    private final FcsUser fcs;
    private Date dateOfBirth;
    private String[] takenListOfBooks;

    public FcsUser getFcs() {
        return new FcsUser (fcs.firstname, fcs.secondname, fcs.surname);
    }

    public Date getDateOfBirth() {
        return new Date(dateOfBirth.getTime());
    }

    public String[] getTakenListOfBooks() {
        return takenListOfBooks;
    }

    public User(FcsUser fcs, Date dateOfBirth, String...takenListOfBooks) {
        this.fcs = fcs;
        this.dateOfBirth = dateOfBirth;
        this.takenListOfBooks = takenListOfBooks;
    }
    public class FcsUser{
        private final String firstname;
        private final String secondname;
        private final String surname;

        public FcsUser(String firstname, String secondname, String surname) {
            this.firstname = firstname;
            this.secondname = secondname;
            this.surname = surname;
        }
    }
}
